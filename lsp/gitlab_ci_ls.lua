vim.lsp.config.gitlab_ci_ls = {
  cmd = { "gitlab-ci-ls" },
  filetypes = { "yaml.gitlab-ci" },
  root_markers = {
    ".gitlab-ci.yml",
    ".gitlab-ci.yaml",
    ".gitlab-ci.json",
    ".gitlab-ci.toml",
    ".gitlab-ci",
    ".git",
  },
  init_options = {
    cache_path = vim.uv.os_homedir() .. "/.cache/gitlab-ci-ls",
    log_path = vim.uv.os_homedir() .. "/.cache/gitlab-ci-ls" .. "/log/gitlab-ci-ls.log",
  },
  docs = {
    description = [[
https://github.com/alesbrelih/gitlab-ci-ls

Language Server for Gitlab CI

`gitlab-ci-ls` can be installed via cargo:
cargo install gitlab-ci-ls
]],
  },
}
