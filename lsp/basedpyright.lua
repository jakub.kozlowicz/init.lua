vim.lsp.config.basedpyright = {
  cmd = { "basedpyright-langserver", "--stdio" },
  filetypes = { "python" },
  root_markers = {
    "pyproject.toml",
    "setup.py",
    "setup.cfg",
    "requirements.txt",
    "Pipfile",
    "pyrightconfig.json",
    ".git",
  },
  settings = {
    basedpyright = {
      analysis = {
        autoSearchPaths = true,
        diagnosticMode = "workspace",
        useLibraryCodeForTypes = true,
      },
    },
  },
  commands = {
    BasedPyrightSetPythonPath = function(command, ctx)
      local client = vim.lsp.get_client_by_id(ctx.client_id)
      if not client then
        return
      end

      if client.settings then
        client.settings.python = vim.tbl_deep_extend("force", client.settings.python or {},
          { pythonPath = command.arguments[0] })
      else
        client.config.settings = vim.tbl_deep_extend("force", client.config.settings,
          { python = { pythonPath = command.arguments[0] } })
      end

      client:notify(vim.lsp.protocol.Methods.workspace_didChangeConfiguration, { settings = nil })
    end,
  },
  docs = {
    description = [[
https://detachhead.github.io/basedpyright

`basedpyright`, a static type checker and language server for python
]],
  },
}
