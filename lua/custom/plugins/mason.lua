return {
  "williamboman/mason.nvim",
  build = ":MasonUpdate",
  config = function()
    local tools = {
      lsp = {
        "basedpyright",
        "bash-language-server",
        "docker-compose-language-service",
        "dockerfile-language-server",
        "gitlab-ci-ls",
        "gopls",
        "golangci-lint",
        "golangci-lint-langserver",
        "json-lsp",
        "lua-language-server",
        "ruff",
        "taplo",
        "yaml-language-server",
      },
      linters = {
        "shellcheck",
      },
      formatters = {},
    }

    require("mason").setup({})

    local mr = require("mason-registry")
    local to_install = vim.iter({ "lsp", "linters", "formatters" })
        :map(function(key)
          return tools[key]
        end)
        :flatten()
        :totable()

    local function ensure_installed()
      for _, tool in ipairs(to_install) do
        local p = mr.get_package(tool)
        if not p:is_installed() then
          p:install()
        end
      end
    end

    if mr.refresh then
      mr.refresh(ensure_installed)
    else
      ensure_installed()
    end

    vim.keymap.set("n", "<leader>cm", "<cmd>Mason<cr>", { desc = "Mason" })

    -- TODO(jakub-kozlowicz): where to put this?
    vim.lsp.enable({
      "basedpyright",
      "bashls",
      "docker_compose_language_service",
      "docker_ls",
      "gitlab_ci_ls",
      "gopls",
      "golangci_lint_ls",
      "jsonls",
      "lua_ls",
      "ruff",
      "taplo",
      "yamlls",
    })
  end,
}
