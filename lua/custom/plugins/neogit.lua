return {
  "NeogitOrg/neogit",
  version = false,
  dependencies = { "nvim-lua/plenary.nvim", version = false },
  config = function()
    local neogit = require("neogit")
    neogit.setup({
      disable_signs = true,
    })

    vim.keymap.set("n", "<leader>gg", function() neogit.open({ cwd = "%:p:h" }) end, { desc = "Open NeoGit" })
  end,
}
