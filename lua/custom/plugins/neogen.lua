return {
  "danymat/neogen",
  config = function()
    local neogen = require("neogen")

    neogen.setup({
      snippet_engine = "nvim",
      enabled = true,
      languages = {
        python = {
          template = {
            annotation_convention = "reST",
          },
        },
      },
    })

    vim.keymap.set("n", "<leader>cn", neogen.generate, { desc = "Generate code documentation" })
  end,
}
