return {
  "nvim-treesitter/nvim-treesitter",
  -- No strict versioning in the repo
  version = false,
  build = ":TSUpdate",
  config = function()
    require("nvim-treesitter.configs").setup({
      ignore_install = {},
      ensure_installed = {
        "bash",
        "c",
        "comment",
        "diff",
        "gitattributes",
        "gitcommit",
        "gitignore",
        "git_config",
        "git_rebase",
        "json",
        "lua",
        "luadoc",
        "markdown",
        "markdown_inline",
        "python",
        "query",
        "regex",
        "toml",
        "vim",
        "vimdoc",
        "yaml",
      },
      auto_install = true,
      highlight = { enable = true },
      indent = { enable = true },
      sync_install = false,
    })
  end,
}
