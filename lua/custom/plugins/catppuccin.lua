return {
  "catppuccin/nvim",
  name = "catppuccin",
  version = false,
  priority = 1000,
  config = function()
    require("catppuccin").setup({
      default_integrations = false,
      integrations = {
        blink_cmp = true,
        bufferline = true,
        fidget = true,
        gitsigns = true,
        harpoon = true,
        markdown = true,
        mason = true,
        native_lsp = {
          enabled = true,
          virtual_text = {
            errors = { "italic" },
            hints = { "italic" },
            warnings = { "italic" },
            information = { "italic" },
            ok = { "italic" },
          },
          underlines = {
            errors = { "underline" },
            hints = { "underline" },
            warnings = { "underline" },
            information = { "underline" },
            ok = { "underline" },
          },
          inlay_hints = {
            background = true,
          },
        },
        neogit = true,
        semantic_tokens = true,
        telescope = {
          enabled = true,
        },
        treesitter = true,
      },
    })

    vim.cmd.colorscheme("catppuccin")
  end,
}
