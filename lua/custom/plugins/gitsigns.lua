return {
  "lewis6991/gitsigns.nvim",
  config = function()
    require("gitsigns").setup({
      signs = {
        add          = { text = "┃" },
        change       = { text = "┃" },
        delete       = { text = "_" },
        topdelete    = { text = "‾" },
        changedelete = { text = "~" },
        untracked    = { text = "┆" },
      },
      signcolumn = true,
      current_line_blame = true,
      on_attach = function(bufnr)
        local gs = require("gitsigns")

        local function map(mode, l, r, opts)
          opts = opts or {}
          opts.buffer = bufnr
          vim.keymap.set(mode, l, r, opts)
        end

        -- Actions
        map("n", "<leader>gs", gs.stage_hunk, { desc = "Git Stage hunk" })
        map("v", "<leader>gs", function() gs.stage_hunk({ vim.fn.line("."), vim.fn.line("v") }) end,
          { desc = "Git Stage hunk" })
        map("n", "<leader>gr", gs.reset_hunk, { desc = "Git Reset hunk" })
        map("v", "<leader>gr", function() gs.reset_hunk({ vim.fn.line("."), vim.fn.line("v") }) end,
          { desc = "Git Reset hunk" })
        map("n", "<leader>gu", gs.undo_stage_hunk, { desc = "Git Undo stage hunk" })
        map("n", "<leader>gp", gs.preview_hunk, { desc = "Git Preview hunk" })
        map("n", "<leader>gd", gs.diffthis, { desc = "Git Diff hunk" })
        map("n", "<leader>gS", gs.stage_buffer, { desc = "Git Stage buffer" })
        map("n", "<leader>gR", gs.reset_buffer, { desc = "Git Reset buffer" })
        map("n", "<leader>gD", function() gs.diffthis("~") end, { desc = "Git Diff buffer" })
        map({ "o", "x" }, "ih", ":<C-U>Gitsigns select_hunk<CR>")
      end
    })
  end,
}
