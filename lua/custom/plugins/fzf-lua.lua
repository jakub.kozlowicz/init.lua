return {
  "ibhagwan/fzf-lua",
  config = function()
    local fzf = require("fzf-lua")
    fzf.setup({})

    fzf.register_ui_select()

    local map = function(mode, lhs, rhs, opts)
      opts = opts or {}
      opts.noremap = true
      opts.silent = true
      vim.keymap.set(mode, lhs, rhs, opts)
    end

    map({ "n" }, "<leader><leader>", fzf.files, { desc = "Find files" })
    map({ "n" }, "<leader>,", fzf.buffers, { desc = "Find existing buffers" })

    map({ "n" }, "<leader>sa", fzf.autocmds, { desc = "Search Autocommands" })
    map({ "n" }, "<leader>sg", fzf.live_grep, { desc = "Search by Grep" })
    map({ "n" }, "<leader>sh", fzf.helptags, { desc = "Search Help" })
    map({ "n" }, "<leader>sk", fzf.keymaps, { desc = "Search Keymaps" })
    map({ "n" }, "<leader>ss", fzf.spell_suggest, { desc = "Search Spelling Sugestions" })
  end
}
