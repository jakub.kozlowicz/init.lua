-- Clear highlights
vim.keymap.set("n", "<esc>", "<cmd>noh<cr>", { desc = "Escape and Clear hlsearch" })

-- Better navigation up and down
vim.keymap.set({ "n", "x" }, "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set({ "n", "x" }, "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
vim.keymap.set({ "n", "x" }, "<Up>", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set({ "n", "x" }, "<Down>", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Resize window using <ctrl> arrow keys
vim.keymap.set("n", "<S-Up>", "<cmd>resize +2<cr>", { desc = "Increase window height" })
vim.keymap.set("n", "<S-Down>", "<cmd>resize -2<cr>", { desc = "Decrease window height" })
vim.keymap.set("n", "<S-Left>", "<cmd>vertical resize -2<cr>", { desc = "Decrease window width" })
vim.keymap.set("n", "<S-Right>", "<cmd>vertical resize +2<cr>", { desc = "Increase window width" })

-- Move Lines up and down
vim.keymap.set("n", "<M-k>", "<cmd>m .-2<cr>==", { desc = "Move up" })
vim.keymap.set("n", "<M-j>", "<cmd>m .+1<cr>==", { desc = "Move down" })
vim.keymap.set("i", "<M-k>", "<esc><cmd>m .-2<cr>==gi", { desc = "Move up" })
vim.keymap.set("i", "<M-j>", "<esc><cmd>m .+1<cr>==gi", { desc = "Move down" })
vim.keymap.set("v", "<M-k>", ":m '<-2<cr>gv=gv", { desc = "Move up" })
vim.keymap.set("v", "<M-j>", ":m '>+1<cr>gv=gv", { desc = "Move down" })

-- Move lines left and right
vim.keymap.set("n", "<M-h>", "<<", { desc = "Move left" })
vim.keymap.set("n", "<M-l>", ">>", { desc = "Move right" })
vim.keymap.set("v", "<M-h>", "<gv", { desc = "Move left" })
vim.keymap.set("v", "<M-l>", ">gv", { desc = "Move right" })
vim.keymap.set("i", "<M-h>", "<esc><<gi", { desc = "Move left" })
vim.keymap.set("i", "<M-l>", "<esc>>>gi", { desc = "Move right" })

-- Diagnostic navigation
vim.keymap.set("n", "<leader>xd", vim.diagnostic.setloclist, { desc = "Document diagnostics" })
vim.keymap.set("n", "<leader>xw", vim.diagnostic.setqflist, { desc = "Workspace diagnostics" })
vim.keymap.set("n", "]q", vim.cmd.cnext, { desc = "Go to next quickfix item" })
vim.keymap.set("n", "[q", vim.cmd.cprevious, { desc = "Go to previous quickfix item" })
vim.keymap.set("n", "]l", vim.cmd.lnext, { desc = "Go to next location list item" })
vim.keymap.set("n", "[l", vim.cmd.lprevious, { desc = "Go to previous location list item" })

-- Explorer
vim.keymap.set("n", "<leader>fe", "<cmd>Explore<cr>", { desc = "Open File Explorer" })

-- Lazy
vim.keymap.set("n", "<leader>l", "<cmd>Lazy<cr>", { desc = "Open Lazy" })
