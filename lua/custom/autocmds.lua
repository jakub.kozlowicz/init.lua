local function augroup(name)
  return vim.api.nvim_create_augroup("custom_" .. name, { clear = true })
end

vim.api.nvim_create_autocmd({ "FocusGained", "TermClose", "TermLeave" }, {
  desc = "Check if we need to reload the file when it changed",
  group = augroup("checktime"),
  callback = function()
    if vim.o.buftype ~= "nofile" then
      vim.cmd.checktime()
    end
  end,
})

vim.api.nvim_create_autocmd({ "TextYankPost" }, {
  desc = "Highlight when yanking (copying) text",
  group = augroup("highlight_yank"),
  callback = function()
    vim.hl.on_yank()
  end,
})

vim.api.nvim_create_autocmd({ "VimResized" }, {
  desc = "Resize splits if window got resized",
  group = augroup("resize_splits"),
  callback = function()
    local current_tab = vim.fn.tabpagenr()
    vim.cmd("tabdo wincmd =")
    vim.cmd("tabnext " .. current_tab)
  end,
})

vim.api.nvim_create_autocmd({ "FileType" }, {
  desc = "Close filetype with <q>",
  group = augroup("close_with_q"),
  pattern = {
    "help",
    "lspinfo",
    "qf",
    "query",
    "startuptime",
    "checkhealth",
  },
  callback = function(event)
    vim.bo[event.buf].buflisted = false
    vim.keymap.set("n", "q", "<cmd>close<cr>", { buffer = event.buf, silent = true })
  end,
})

vim.api.nvim_create_autocmd({ "FileType" }, {
  desc = "Easier closing man-files open inline",
  group = augroup("man_unlisted"),
  pattern = { "man" },
  callback = function(event)
    vim.bo[event.buf].buflisted = false
  end,
})

vim.api.nvim_create_autocmd({ "FileType" }, {
  desc = "Wrap lines and check spell in text files",
  group = augroup("wrap_spell"),
  pattern = { "gitcommit", "markdown", "text", "tex", "plaintex" },
  callback = function()
    vim.opt_local.wrap = true
    vim.opt_local.linebreak = true
    vim.opt_local.spell = true
  end,
})

vim.api.nvim_create_autocmd({ "BufWritePre" }, {
  desc = "Create intermediate directories if not present when saving file",
  group = augroup("auto_create_dir"),
  callback = function(event)
    if event.match:match("^%w%w+:[\\/][\\/]") then
      return
    end
    local file = vim.uv.fs_realpath(event.match) or event.match
    vim.fn.mkdir(vim.fn.fnamemodify(file, ":p:h"), "p")
  end,
})

vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
  desc = "Set filetype for gitlab files",
  group = augroup("gitlab_filetype"),
  pattern = ".gitlab*",
  callback = function()
    vim.bo.filetype = "yaml.gitlab"
  end,
})

vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile" }, {
  desc = "Set filetype for docker-compose files",
  group = augroup("docker_compose_filetype"),
  pattern = { "compose*", "docker-compose*" },
  callback = function()
    vim.bo.filetype = "yaml.docker-compose"
  end,
})

vim.api.nvim_create_autocmd("LspAttach", {
  desc = "Setup LSP functionality for attached clients",
  group = vim.api.nvim_create_augroup("custom-lsp-attach", { clear = true }),
  callback = function(args)
    local map = function(mode, keys, func, desc)
      vim.keymap.set(mode, keys, func, { buffer = args.buf, desc = desc })
    end

    -- mapped by default to <C-S> (only insert mode)
    -- map({ "n" }, "gK", vim.lsp.buf.signature_help, "Signature help")
    -- mapped by default to grr
    -- map({ "n" }, "gr", vim.lsp.buf.references, "Goto References")
    -- mapped by default to gri
    -- map({ "n" }, "gi", vim.lsp.buf.implementation, "Goto Implementations")
    -- mapped by default to gra
    -- map({ "n" }, "<leader>ca", vim.lsp.buf.code_action, "Code Action")
    -- mapped by default to grn
    -- map({ "n" }, "<leader>cr", vim.lsp.buf.rename, "Code Rename")

    map({ "n" }, "grd", vim.lsp.buf.definition, "Goto Definition")
    map({ "n" }, "grD", vim.lsp.buf.declaration, "Goto Declarations")
    map({ "n" }, "grt", vim.lsp.buf.type_definition, "Goto Type definition")
    map({ "n" }, "grc", vim.lsp.codelens.run, "Run CodeLens")
    map({ "n" }, "grf", vim.lsp.buf.format, "Code Format")

    local client = vim.lsp.get_client_by_id(args.data.client_id)
    local methods = vim.lsp.protocol.Methods

    if not client then
      return
    end

    -- document highlights
    if client:supports_method(methods.textDocument_documentHighlight, args.buf) then
      vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
        buffer = args.buf,
        callback = vim.lsp.buf.document_highlight,
      })

      vim.api.nvim_create_autocmd({ "CursorMoved", "CursorMovedI" }, {
        buffer = args.buf,
        callback = vim.lsp.buf.clear_references,
      })
    end

    -- codelens
    if client:supports_method(methods.textDocument_codeLens, args.buf) then
      vim.api.nvim_create_autocmd({ "BufEnter", "CursorHold", "InsertLeave" }, {
        buffer = args.buf,
        callback = function()
          vim.lsp.codelens.refresh({ bufnr = args.buf })
        end,
      })
    end

    -- inlay hints
    if client:supports_method(methods.textDocument_inlayHint, args.buf) then
      vim.lsp.inlay_hint.enable(true, { bufnr = args.buf })
    end

    -- formatting
    if client:supports_method(methods.textDocument_formatting, args.buf) then
      vim.api.nvim_create_autocmd("BufWritePre", {
        buffer = args.buf,
        callback = function()
          vim.lsp.buf.format({ bufnr = args.buf, id = client.id })
          -- HACK: Some LSP servers do not provide diagnostics after formating
          -- when there is no change in the file content. This is a workaround
          -- to force the diagnostics to be updated after formatting.
          -- https://github.com/neovim/neovim/issues/25014#issuecomment-2312672119
          vim.diagnostic.enable()
        end,
      })
    end

    -- Do not enable this for now because it folds everything on save and it's annoying
    -- foldexpr
    -- if client:supports_method(methods.textDocument_foldingRange) then
    --   local win = vim.api.nvim_get_current_win()
    --   vim.wo[win][0].foldmethod = "expr"
    --   vim.wo[win][0].foldexpr = "v:lua.vim.lsp.foldexpr()"
    -- end

    -- The blink is better than the default one so do not enable this for now
    -- completion
    -- if client:supports_method(methods.textDocument_completion) then
    --   vim.lsp.completion.enable(true, client.id, args.buf, { autotrigger = true })
    -- end
  end,
})

vim.api.nvim_create_autocmd("LspDetach", {
  desc = "Cleanup LSP functionality for detached clients",
  callback = function(args)
    local client = vim.lsp.get_client_by_id(args.data.client_id)
    local methods = vim.lsp.protocol.Methods

    if not client then
      return
    end

    -- document highlights
    if client:supports_method(methods.textDocument_documentHighlight, args.buf) then
      vim.lsp.buf.clear_references()
      vim.api.nvim_clear_autocmds({
        event = { "CursorHold", "CursorHoldI", "CursorMoved", "CursorMovedI" },
        buffer = args.buf
      })
    end

    -- codelens
    if client:supports_method(methods.textDocument_codeLens, args.buf) then
      vim.lsp.codelens.clear(client.id, args.buf)
      vim.api.nvim_clear_autocmds({
        event = { "BufEnter", "CursorHold", "InsertLeave" },
        buffer = args.buf,
      })
    end

    -- inlay hints
    if client:supports_method(methods.textDocument_inlayHint, args.buf) then
      vim.lsp.inlay_hint.enable(false, { bufnr = args.buf })
    end

    -- formatting
    if client:supports_method(methods.textDocument_formatting, args.buf) then
      vim.api.nvim_clear_autocmds({
        event = "BufWritePre",
        buffer = args.buf,
      })
    end
  end,
})
